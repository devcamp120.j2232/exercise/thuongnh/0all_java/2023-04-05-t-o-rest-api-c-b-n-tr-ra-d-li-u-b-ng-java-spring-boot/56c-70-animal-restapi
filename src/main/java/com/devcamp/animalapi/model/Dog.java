package com.devcamp.animalapi.model;



public class Dog  extends Mammal{

    public Dog(String name) {
        super(name);
    }
    
    public void greets(){
        System.out.println("woof");
    }
    
    public String getName(){
        return name;
     }
    // nếu truyền thêm 1 con chó nữa
    public void greets(Dog another){
        System.out.println("woooof");
    }

    @Override
    public String toString() {
        return String.format("Dog [Mammal[Amial [Name = %s]]]", name);
    }

    
}
